# Prerequisites

## Installation Ansible
Follow these steps to install Ansible on your system:
### Step 1: Install Python
If you don't have Python installed on your system, you can download and install Python from the [Python.org](https://Python.org) website . Choose the appropriate version for your operating system (Windows, macOS, or Linux) and follow the installation instructions provided on the website.
### Step 2: Install Ansible using pip
Once you have Python installed, you can use pip to install Ansible. Open a terminal or command prompt and enter the following command:
```bash
$ pip install ansible
```
### Step 3: Verify Installation
Once the Ansible installation is complete, you can test the installation by using the following command
```bash
$ ansible --version
```
### Step 4: Verify Connectivity to the Server
Before running Ansible playbook, it's essential to ensure that the host machine can connect to the server where Ansible playbook will be executed. You can verify the connectivity using the ping command. Replace <server_ip> with the IP address of your server.

```bash
$ ping <server_ip>
```
If the ping is successful, it means that the host machine can communicate with the server.
## Create Training folder.
Open a terminal or command prompt and enter the following command to create a folder named "ansible_training" and navigate to the newly created folder
```bash
$ mkdir ansible_training && cd ansible_training
```