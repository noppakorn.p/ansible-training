### Inventory file
Inventories organize managed nodes in centralized files that provide Ansible with system information and network locations. Using an inventory file, Ansible can manage a large number of hosts with a single command.

Create a directory named **inventories**
```bash
# Create directoy
$ mkdir inventories

```
## Creating a Custom Inventory File as a INI Format
### Step 1 Creating a Custom Inventory File 
1. Create inventory file
```bash
# Create inventory file
$ touch inventories/inventory.ini
```
2. Add a new server to the **inventories/inventory.ini**

```ini
# inventories/inventory.ini
prd-1 ansible_user=user ansible_password=password ansible_host=10.10.10.1
prd-2 ansible_user=user ansible_password=password ansible_host=10.10.10.2
dev-1 ansible_user=user ansible_password=password ansible_host=10.10.10.2

```

3. Verify your inventory.
```bash
$ ansible-inventory -i inventories/inventory.ini --list
```
<details>

<summary>Output should be like this</summary>

```json
{
    "_meta": {
        "hostvars": {
            "dev-1": {
                "ansible_host": "10.10.10.2",
                "ansible_user": "user",
                "ansible_password": "password"
            },
            "prd-1": {
                "ansible_host": "10.10.10.1",
                "ansible_user": "user",
                "ansible_password": "password"
            },
            "prd-2": {
                "ansible_host": "10.10.10.2",
                "ansible_user": "user",
                "ansible_password": "password"
            }
        }
    },
    "all": {
        "children": [
            "ungrouped"
        ]
    },
    "ungrouped": {
        "hosts": [
            "prd-1",
            "prd-2",
            "dev-1"
        ]
    }
}
```
</details>

### Step 2 : Organizing Servers Into Groups.
1. Edit **inventories/inventory.ini** organizing servers into groups.
```ini
# inventories/inventory.ini
[production]
prd-1 ansible_user=user ansible_password=password ansible_host=10.10.10.1
prd-2 ansible_user=user ansible_password=password ansible_host=10.10.10.2

[development]
dev-1 ansible_user=user ansible_password=password ansible_host=10.10.10.2
```
2. Verify your inventory.
```bash
$ ansible-inventory -i inventories/inventory.ini --list
```
<details>

<summary>Output should be like this</summary>

```json
{
    "_meta": {
        "hostvars": {
            "dev-1": {
                "ansible_host": "10.10.10.2",
                "ansible_user": "user",
                "ansible_password": "password"
            },
            "prd-1": {
                "ansible_host": "10.10.10.1",
                "ansible_user": "user",
                "ansible_password": "password"
            },
            "prd-2": {
                "ansible_host": "10.10.10.2",
                "ansible_user": "user",
                "ansible_password": "password"
            }
        }
    },
    "all": {
        "children": [
            "ungrouped",
            "production",
            "development"
        ]
    },
    "development": {
        "hosts": [
            "dev-1"
        ]
    },
    "production": {
        "hosts": [
            "prd-1",
            "prd-2"
        ]
    }
}
```
</details>

### Step 3 : Connection verify
1. Edit your server in **inventory.ini** add your actual server host and delete all mockup server.
```ini
[training]
training-1 ansible_user=<SERVER_HOST_USERNAME> ansible_password=<SERVER_HOST_password> ansible_host=<SERVER_HOST_IP>
```
2. Verify Connection with ping command
```bash
$ ansible training -m ping -i inventories/inventory.ini
```
<details>

<summary>Output should be like this</summary>

```json
training-1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```
</details>

## Creating a Custom Inventory File as a YAML Format
### Step 1 Creating a Custom Inventory File 
1. Create inventory file
```bash
# Create inventory file
$ touch inventories/inventory.yaml
```
2. Add a new server to the **inventories/inventory.yaml**

```yaml
# inventories/inventory.yaml
all:
  children:
    ungrouped:
      hosts:
        prd-1:
          ansible_host: 10.10.10.1
          ansible_user: user
          ansible_password: password
        prd-2:
          ansible_host: 10.10.10.2
          ansible_user: user
          ansible_password: password
        dev-1:
          ansible_host: 10.10.10.3
          ansible_user: user
          ansible_password: password

```

3. Verify your inventory.
```bash
$ ansible-inventory -i inventories/inventory.yaml --list
```
<details>

<summary>Output should be like this</summary>

```json
{
    "_meta": {
        "hostvars": {
            "dev-1": {
                "ansible_host": "10.10.10.2",
                "ansible_user": "user",
                "ansible_password": "password"
            },
            "prd-1": {
                "ansible_host": "10.10.10.1",
                "ansible_user": "user",
                "ansible_password": "password"
            },
            "prd-2": {
                "ansible_host": "10.10.10.2",
                "ansible_user": "user",
                "ansible_password": "password"
            }
        }
    },
    "all": {
        "children": [
            "ungrouped"
        ]
    },
    "ungrouped": {
        "hosts": [
            "prd-1",
            "prd-2",
            "dev-1"
        ]
    }
}
```
</details>

### Step 2 : Organizing Servers Into Groups.
1. Edit **inventories/inventory.yaml** organizing servers into groups.
```yaml
# inventories/inventory.yaml
all:
  children:

    production:
      hosts:
        prd-1:
          ansible_host: 10.10.10.1
          ansible_user: user
          ansible_password: password
        prd-2:
          ansible_host: 10.10.10.2
          ansible_user: user
          ansible_password: password
          
    development:
      hosts:
        dev-1:
          ansible_host: 10.10.10.3
          ansible_user: user
          ansible_password: password
```
2. Verify your inventory.
```bash
$ ansible-inventory -i inventories/inventory.yaml --list
```
<details>

<summary>Output should be like this</summary>

```json
{
    "_meta": {
        "hostvars": {
            "dev-1": {
                "ansible_host": "10.10.10.2",
                "ansible_user": "user",
                "ansible_password": "password"
            },
            "prd-1": {
                "ansible_host": "10.10.10.1",
                "ansible_user": "user",
                "ansible_password": "password"
            },
            "prd-2": {
                "ansible_host": "10.10.10.2",
                "ansible_user": "user",
                "ansible_password": "password"
            }
        }
    },
    "all": {
        "children": [
            "ungrouped",
            "production",
            "development"
        ]
    },
    "development": {
        "hosts": [
            "dev-1"
        ]
    },
    "production": {
        "hosts": [
            "prd-1",
            "prd-2"
        ]
    }
}
```
</details>

### Step 3 : Connection verify
1. Edit your server in **inventories/inventory.yaml** add your actual server host and delete all mockup server.
```yaml
# inventories/inventory.yaml
all:
  children:
    training:
      hosts:
        training-1:
          ansible_host: <SERVER_HOST_IPE>
          ansible_user: <SERVER_HOST_USERNAME>
          ansible_password: <SERVER_HOST_PASSWORD>
```
2. Verify Connection with ping command
```bash
$ ansible training -m ping -i inventories/inventory.yaml
```
<details>

<summary>Output should be like this</summary>

```json
training-1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}
```
</details>


