# Practice install Nginx with index.html
## Setup Environment
### Create index.html with text “Hello World” inside
Create **index.html** with text “Hello World” inside
```bash
$ touch index.html
```
```html
# index.html
<!DOCTYPE html>
<html>
<head>
  <title>Hello World</title>
</head>
<body>
  <h1>Hello World</h1>
</body>
</html>
```

## Nginx Install Playbook
### Create nginx-install.yaml playbook
1. Create **nginx-install.yaml** playbook and install nginx with package module
```bash
$ touch tasks/nginx-install.yaml
```
```yaml
# nginx-install.yaml
- name: Install Nginx
  hosts: all
  become: true
  tasks:
    - name: Install Nginx
      ansible.builtin.package:
        name: nginx
        state: latest
```
2. Copy your **index.html** to **/var/www/html/index.html** with copy module
```yaml
# nginx-install.yaml
- name: Install Nginx
  hosts: all
  become: true
  tasks:
    - name: Install Nginx
      ansible.builtin.package:
        name: nginx
        state: latest

    - name: Copy index.html # Add task Copy
      ansible.builtin.copy:
        src: "../index.html"
        dest: "/var/www/html/index.html"
```
3. Run Ansible Playbook command
```bash
$ ansible-playbook -i inventories/inventory.ini tasks/nginx-install.yaml
```
4. access web

## Use Variables with Nginx Playbook
There are two different methods of declaring variables in Ansible playbook: directly within the playbook and by including variables from a separate file.
### Method 1 : Use Direct Variable Declaration
Modify **tasks/nginx-install.yaml** file to use direct variable.
```yaml
# tasks/nginx-install.yaml
- name: Install Nginx
  hosts: all
  become: true
  tasks:
    - name: Install Nginx
      ansible.builtin.package:
        name: nginx
        state: latest

    - name: Copy index.html
      ansible.builtin.copy:
        src: "../{{ index_filename }}"
        dest: "{{ index_path }}/{{ index_filename }}"

  vars: 
    index_filename: index.html # use direct variable
    index_path: /var/www/html # use direct variable
```
### Method 2 : Including Variables from File
1. Create a variable file Most of them are used as **defaults/main.yaml**
```bash
$ mkdir defaults
$ touch defaults/main.yaml
```
2. Define the variables **index_filename** and **index_path** in **defaults/main.yaml**
```yaml
# defaults/main.yaml
index_filename: index.html
index_path: /var/www/html
```
3. Modify **tasks/nginx-install.yaml** file to include variables from the file
```yaml
# tasks/nginx-install.yaml
- name: Install Nginx
  hosts: all
  become: true
  tasks:
    - name: Install Nginx
      ansible.builtin.package:
        name: nginx
        state: latest

    - name: Copy index.html
      ansible.builtin.copy:
        src: "../{{ index_filename }}"
        dest: "{{ index_path }}/{{ index_filename }}"

  vars_files:
    - "../defaults/main.yaml" #variables from the file
    
```

## Use Template with Nginx Playbook
1. Create a template directory and template file file
```bash
$ mkdir templates
$ touch templates/index.html.j2
```
```jinja
# templates/index.html.j2
<p>Hello {{ ansible_hostname }}</p>
<p>This is private ip address: {{ ansible_default_ipv4['address'] }}</p>
<p>This is gateway ip address: {{ ansible_default_ipv4['gateway'] }}</p>
<p>This is OS: {{ ansible_distribution }} {{ ansible_distribution_version }}</p>
```

2. Modify **tasks/nginx-install.yaml** file to use template
```yaml
# tasks/nginx-install.yaml
- name: Install Nginx
  hosts: all
  become: true
  tasks:
    - name: Install Nginx
      ansible.builtin.package:
        name: nginx
        state: latest

    - name: Copy index.html
      ansible.builtin.template: # use template module
        src: "../templates/{{ index_filename }}.j2" # template src
        dest: "{{ index_path }}/{{ index_filename }}" 

  vars_files:
    - "../defaults/main.yaml"
    
```
3. Run Ansible Playbook command
```bash
$ ansible-playbook -i inventories/inventory.ini tasks/nginx-install.yaml
```