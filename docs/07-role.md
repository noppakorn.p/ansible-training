# Creating an Ansible Role for Nginx Configuration
In this guide, we'll walk through the steps of creating an Ansible role for configuring Nginx, a popular web server and reverse proxy.
## 1. Create Roles Directory
```bash
$ mkdir roles 
```
## 2. Create the Role Structure

Use the `ansible-galaxy init` command to create the structure for our Nginx role.

```bash
$ cd roles
$ ansible-galaxy init install-nginx
```

## 3. Edit Tasks
Navigate to the `tasks directory` inside your role and edit the `roles/install-nginx/tasks/main.yml` file to include the tasks for installing and starting Nginx.

```yaml
# roles/install-nginx/tasks/main.yaml
- name: Install Nginx
  hosts: all
  become: true
  tasks:
    - name: Install Nginx
      ansible.builtin.package:
        name: nginx
        state: latest

    - name: Copy index.html
      ansible.builtin.template:
        src: "../template/{{ index_filename }}.j2"
        dest: "{{ index_path }}/{{ index_filename }}"
```

## 4. Edit Defaults
To set default variables for your role, you can edit the `roles/install-nginx/defaults/main.yml` file.
```yaml
# roles/install-nginx/default/main.yaml
index_filename: index.html
index_path: /var/www/html
```
## 5. Edit Templates
To set template for your role, you can create the `roles/install-nginx/defaults/index.html.j2` file.
```bash
$ touch install-nginx/templates/index.html.j2
```
```jinja
# roles/install-nginx/templates/index.html.j2
<p>Hello {{ ansible_hostname }}</p>
<p>This is private ip address: {{ ansible_default_ipv4['address'] }}</p>
<p>This is gateway ip address: {{ ansible_default_ipv4['gateway'] }}</p>
<p>This is OS: {{ ansible_distribution }} {{ ansible_distribution_version }}</p>
```

## 6. Write a Playbook Using the Role
Create a playbook that uses the Nginx role we just created.
```bash
# Back to main Directory
$ cd .. 
$ touch playbook.yaml
```
```yaml
# playbook.yaml
---
- name: Apply Nginx role
  hosts: all
  roles:
    - install-nginx
```
## Run the Playbook
Before running Ansible Playbook make sure you have directory structure like this
```md
inventories
└── inventory.ini
roles
└── install-nginx
    ├── defaults
    │   └── main.yaml
    ├── meta
    │   └── main.yaml
    ├── tasks
    │   └── main.yaml
    └── templates
        └── index.html.j2
playbook.yaml
```
```bash 
$ ansible-playbook inventories/inventory.ini playbook.yaml
```

