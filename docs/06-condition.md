# Ansible Playbook with "when" Condition
In Ansible, the "when" condition allows you to control whether or not a task should be executed based on certain conditions. Here's how you can incorporate "when" conditions into your Ansible Playbooks

```yaml
---
# tasks/when-condition.yaml
- name: Install Apache
  hosts: all
  tasks:
    - name: Install Apache on Red Hat
      yum:
        name: httpd
        state: present
      when: ansible_distribution == 'RedHat'

    - name: Install Apache on Ubuntu
      apt:
        name: apache2
        state: present
      when: ansible_distribution == 'Ubuntu'

    # Add more conditions here if needed
    # - name: Additional Task
    #   <module_name>:
    #     <module_arguments>
    #   when: <condition>
```
In this playbook:

* We have two tasks to install Apache, one for Red Hat and one for Ubuntu systems, using the yum module for Red Hat and the apt module for Ubuntu.
* We use the when condition to check the operating system specified in the ansible_distribution variable and install Apache only on the specified operating systems.

Therefore, if the system is Red Hat, Apache will be installed using yum, and if the system is Ubuntu, Apache will be installed using apt. Using when is an effective tool to control Ansible actions based on different conditions efficiently and flexibly.