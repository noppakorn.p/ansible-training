# Loop with Ansible Playbook
In Ansible, loops are powerful constructs for iterating over a set of data and executing tasks repetitively based on that data. Below are examples of Ansible Playbooks demonstrating the use of loops in different scenarios.

### Loops Items
This playbook demonstrates how to create a local group named LocalGroup and add two local users opsta1 and opsta2 with the same password.
```yaml
# tasks/loop-items.yaml
- name: Linux Preparation
  hosts: all
  become: true
  tasks:
    - name: Create local group to contain new users
      group:
        name: LocalGroup
        state: present
    - name: Create local user
      user:
        name: "{{ item }}" # usage
        password: "P@ssw0rd"
        groups: LocalGroup
        update_password: always
      loop: # Loops items
        - opsta1
        - opsta2
        - opsta3
    - name: Echo created users
      debug:
        msg: "Created user: {{ item }}" # usage
      loop: # Loops items
        - opsta1
        - opsta2
        - opsta3
```


### Loops Dictionary
In this playbook, we define users and their passwords using a dictionary format. This allows for more flexibility in defining user-specific attributes.
```yaml
# tasks/loop-dictionary.yaml
- name: Linux Preparation
  hosts: all
  become: true
  tasks:
    - name: Create local group to contain new users
      group:
        name: LocalGroup
        state: present

    - name: Create local user
      user:
        name: "{{ item.name }}"  # usage
        password: "{{ item.password }}"  # usage
        groups: LocalGroup
        update_password: always
      loop: # Loops dictionary
        - name: opsta1
          password: P@ssw0rd
        - name: opsta2
          password: P@ssw0rd
        - name: opsta3
          password: P@ssw0rd

    - name: Echo created users
      debug:
        msg: "Created user: {{ item.name }} with password: {{ item.password }}"  # usage
      loop: # Loops dictionary
        - name: opsta1
          password: P@ssw0rd
        - name: opsta2
          password: P@ssw0rd
        - name: opsta3
          password: P@ssw0rd
```

### Loops with Variables
This playbook demonstrates how to use a list of dictionaries stored in a variable users to create local users. This approach allows for easy management and modification of user data.
```yaml
# tasks/loop-variables.yaml
- name: Linux Preparation
  hosts: all
  become: true
  tasks:
    - name: Create local group to contain new users
      group:
        name: LocalGroup
        state: present
    - name: Create local user
      user:
        name: "{{ item.name }}" # usage
        password: "{{ item.password }}" # usage
        groups: LocalGroup
        update_password: always
      loop: "{{ users }}" # Loops Variables
    - name: Echo created users
      debug:
        msg: "Created user: {{ item.name }} with password: {{ item.password }}" # usage
      loop: "{{ users }}" # Loops Variables
  vars:
    users:
      - name: opsta1
        password: P@ssw0rd
      - name: opsta2
        password: P@ssw0rd
      - name: opsta3
        password: P@ssw0rd
```
