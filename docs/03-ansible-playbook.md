### Creating a Playbook File 
Create a directory named **tasks**
```bash
# Create directoy
$ mkdir tasks
```

## Ansible Playbook install Apache
1. Create tasks/apache-install.yaml file in ansible_training
```bash
# Create apache-install.yaml file
$ touch tasks/apache-install.yaml
```
```yaml
# tasks/apache-install.yaml
---
- name: Install Apache
  hosts: all
  become: true
  tasks:
    - name: Install the latest version of Apache
      ansible.builtin.package:
        name:
          - httpd
        state: latest

    - name: Start Apache service
      ansible.builtin.service:
        name: apache2
        state: started
```

2. Run Ansible Playbook command
```bash
$ ansible-playbook -i inventories/inventory.ini tasks/apache-install.yaml
```

## Ansible Playbook uninstall Apache
1. Create tasks/apache-remove.yaml file in ansible_training
```bash
# Create apache-install.yaml file
$ touch tasks/apache-remove.yaml
```
```yaml
# tasks/apache-remove.yaml
---
- name: Remove Apache
  hosts: all
  become: true
  tasks:
    - name: Remove Apache
      package:
        name: apache2
        state: absent
```

2. Run Ansible Playbook command
```bash
$ ansible-playbook -i inventories/inventory.ini tasks/apache-remove.yaml
```